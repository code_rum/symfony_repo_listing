<?php

/*
 * @author Dharmesh Mallah
 */

namespace SymfonyRepoListing\App;

use Github\Client;

class SymfonyRepoList
{
    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Sets organization
     *
     * @param Name of organization
     */
    public function setRepo($repository)
    {
        $this->repo = $repository;
    }

    /**
     * Gets complete list of repositories from organization
     *
     * @return array Complete list of repos hosted under organization
     */
    public function pourList()
    {
        $repoList = array();
        $page = 1; 

        while(true) {
            $repoInfo = $this->client->api('organization')->repositories($this->repo, 'all', $page);

            if($repoInfo) {
                $getFullName = array_map(
                    array($this, "getFullName"), 
                    $repoInfo
                );
                $repoList = array_merge(
                    $repoList, 
                    $getFullName
                );
                $page += 1;
            } else {
                return $repoList;
            }
        }
    }
    
    /**
     * Gets repo full name from array
     *
     * @param array $getInfo Repo info fetched from organization
     *
     * @return string|Full name of repository
     */
    private function getFullName($getInfo)
    {
       return $getInfo['full_name'];
    }
}
